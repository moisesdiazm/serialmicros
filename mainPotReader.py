from PyQt4 import QtCore, QtGui
import sys
import serial
from PotReader import Ui_MainWindow

from rxThread import RxThread

# serTx = serial.Serial(
#     #port='/dev/ttyUSB0',
#     port='/dev/pts/2',
#     baudrate=9600,
#     parity=serial.PARITY_NONE,
#     stopbits=serial.STOPBITS_ONE,
#     bytesize=serial.EIGHTBITS
# )

serRx = serial.Serial(
    port='/dev/ttyUSB2',
    # port='/dev/pts/4',
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)


class MyForm(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.horizontalSlider.valueChanged.connect(self.valueChangeSlider)
        self.rxThread = RxThread(serRx,self.ui)
        self.rxThread.start()
    
    def valueChangeSlider(self):
        valueSend = int(self.ui.horizontalSlider.value()/99.0*255.0)
        strSend = (valueSend).to_bytes(1, byteorder='big') + b"\n" 
        # serTx.write(strSend)
        # self.ui.dial.setValue(self.ui.horizontalSlider.value())
        # print(valueSend)


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    mainwin = MyForm()
    mainwin.show()
    # serTx.isOpen()
    serRx.isOpen()
    sys.exit(app.exec_())
    rxThread.deactivate()
    rxThread.join()
    # serTx.close()
    serRx.close()