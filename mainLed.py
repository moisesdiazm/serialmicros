from PyQt4 import QtCore, QtGui
import sys
from LedOnOff import Ui_MainWindow
import serial

#  configure the serial connections (the parameters differs on the device you are connecting to)
ser = serial.Serial(
    port='/dev/ttyACM0',
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)


class MyForm(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.pushButton.clicked.connect(self.printOn)
        self.ui.pushButton_2.clicked.connect(self.printOff)
    
    def printOn(self):
        ser.write(b"H")
        print("Button ON")

    def printOff(self):
        ser.write(b"L")
        print("Button OFF")


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    mainwin = MyForm()
    ser.isOpen()
    mainwin.show()
    sys.exit(app.exec_())
    ser.close()

    