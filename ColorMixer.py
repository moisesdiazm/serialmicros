# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ColorMixer.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(492, 398)
        MainWindow.setAutoFillBackground(False)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.colorBox = QtGui.QFrame(self.centralwidget)
        self.colorBox.setGeometry(QtCore.QRect(0, 160, 491, 181))
        self.colorBox.setAutoFillBackground(False)
        self.colorBox.setFrameShape(QtGui.QFrame.StyledPanel)
        self.colorBox.setFrameShadow(QtGui.QFrame.Raised)
        self.colorBox.setObjectName(_fromUtf8("colorBox"))
        self.red = QtGui.QSlider(self.centralwidget)
        self.red.setGeometry(QtCore.QRect(60, 40, 160, 18))
        self.red.setOrientation(QtCore.Qt.Horizontal)
        self.red.setObjectName(_fromUtf8("red"))
        self.green = QtGui.QSlider(self.centralwidget)
        self.green.setGeometry(QtCore.QRect(60, 80, 160, 18))
        self.green.setOrientation(QtCore.Qt.Horizontal)
        self.green.setObjectName(_fromUtf8("green"))
        self.blue = QtGui.QSlider(self.centralwidget)
        self.blue.setGeometry(QtCore.QRect(60, 120, 160, 18))
        self.blue.setOrientation(QtCore.Qt.Horizontal)
        self.blue.setObjectName(_fromUtf8("blue"))
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 492, 27))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))

