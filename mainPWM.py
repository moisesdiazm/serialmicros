from PyQt4 import QtCore, QtGui
import sys
from SliderPWM import Ui_MainWindow
import serial

ser = serial.Serial(
    port='/dev/ttyUSB2',
    # port='/dev/pts/2',
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)

class MyForm(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.horizontalSlider.valueChanged.connect(self.valueChangeSlider)
    
    def valueChangeSlider(self):
        valueSend = int(self.ui.horizontalSlider.value()/99.0*255.0)
        ser.write((valueSend).to_bytes(1, byteorder='big'))
        # print(valueSend)


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    mainwin = MyForm()
    mainwin.show()
    ser.isOpen()
    sys.exit(app.exec_())
    ser.close()

    