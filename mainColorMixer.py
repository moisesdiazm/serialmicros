from PyQt4 import QtCore, QtGui
import sys
import serial
from ColorMixer import Ui_MainWindow

from rxThreadColor import RxThread

# serTx = serial.Serial(
#     #port='/dev/ttyUSB0',
#     port='/dev/pts/2',
#     baudrate=9600,
#     parity=serial.PARITY_NONE,
#     stopbits=serial.STOPBITS_ONE,
#     bytesize=serial.EIGHTBITS
# )

serRx = serial.Serial(
    port='/dev/ttyUSB0',
    #port='/dev/pts/4',
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)


class MyForm(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.red.valueChanged.connect(self.valueChangeSlider)
        self.ui.green.valueChanged.connect(self.valueChangeSlider)
        self.ui.blue.valueChanged.connect(self.valueChangeSlider)
        self.rxThread = RxThread(ser = serRx)
        self.rxThread.start()
        self.connect(self.rxThread, QtCore.SIGNAL("colorupdate"), self.updateColor)
    
    def valueChangeSlider(self):
        redColor = int(self.ui.red.value()/99.0*255.0)
        greenColor = int(self.ui.green.value()/99.0*255.0)
        blueColor = int(self.ui.blue.value()/99.0*255.0)
        # self.ui.colorBox.setStyleSheet("background-color:rgb({},{},{})".format(redColor,greenColor,blueColor));
        # strSend = (redColor).to_bytes(1, byteorder='big') + b"," + (greenColor).to_bytes(1, byteorder='big')  + b"," + (blueColor).to_bytes(1, byteorder='big') + b"\n" 
        # serTx.write(strSend)
    
    def updateColor(self, values):
        self.ui.colorBox.setStyleSheet("background-color:rgb({},{},{})".format(*values))


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    mainwin = MyForm()
    mainwin.show()
    # serTx.isOpen()
    serRx.isOpen()
    sys.exit(app.exec_())
    rxThread.deactivate()
    rxThread.join()
    # serTx.close()
    serRx.close()