from threading import Thread
from time import sleep

class RxThread(Thread):

    def __init__(self, ser, ui):
        Thread.__init__(self)
        self.ui = ui
        self.ser = ser
        self.active = True
        
    def run(self):
        print(" > Starting Serial Rx Thread")

        while self.active:
            msg = self.ser.readline()
            if msg and len(msg)>=4:
                value = (msg[-2]-48)*1.00 + (msg[-3]-48)*10.00 + (msg[-4]-48)*100.00
                intVal = value*100//255
                print(intVal)
                self.ui.dial.setValue(intVal)
            else:
                pass
            
    def deactivate(self):
        print(" > Stopping Serial Rx Thread")
        self.active = False
