from PyQt4 import QtCore
from threading import Thread
from time import sleep

class RxThread(QtCore.QThread):

    def __init__(self, ser, parent=None):
        super(RxThread, self).__init__(parent)
        self.active = True
        self.ser = ser
        
    def run(self):
        print(" > Starting Serial Rx Thread")

        while self.active:
            msg = self.ser.readline()
            if msg:
                valuesList = msg[:-1].split(b",")
                if len(valuesList) >= 3 and len(valuesList[0]) >= 3 and len(valuesList[1]) >= 3 and len(valuesList[2]) >= 3:
                    # print(valuesList)
                    redColor = ((valuesList[0][2]-48)*1.00 + (valuesList[0][1]-48)*10.00 + (valuesList[0][0]-48)*100.00)/255*100
                    greenColor = ((valuesList[1][2]-48)*1.00 + (valuesList[1][1]-48)*10.00 + (valuesList[1][0]-48)*100.00)/255*100
                    blueColor = ((valuesList[2][2]-48)*1.00 + (valuesList[2][1]-48)*10.00 + (valuesList[2][0]-48)*100.00)/255*100
                    print("{},{},{}".format(redColor, greenColor, blueColor))
                    self.emit(QtCore.SIGNAL("colorupdate"),(redColor, greenColor, blueColor))
                    sleep(0.01)
            else:
                pass
            
    def deactivate(self):
        print(" > Stopping Serial Rx Thread")
        self.active = False
